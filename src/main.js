// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

console.log({ 
    sum: mylib.sum(1, 1),
    subtract: mylib.subtract(5, 1),
    random: mylib.random(),
    array: mylib.arrayGen()
});

app.get('/add', (req, res) => { // http://localhost:3000/add?a=2&b=3
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.sum(a,b));
});

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
});